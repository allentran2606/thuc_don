export function layThongTinTuForm() {
  let ma = document.getElementById(`foodID`).value;
  let ten = document.getElementById(`tenMon`).value;
  let loai = document.getElementById(`loai`).value;
  let gia = document.getElementById(`giaMon`).value;
  let khuyenMai = document.getElementById(`khuyenMai`).value;
  let tinhTrang = document.getElementById(`tinhTrang`).value;
  let hinhMon = document.getElementById(`hinhMon`).value;
  let moTa = document.getElementById(`moTa`).value;

  return {
    ma,
    ten,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
}

export default function showThongTinLenForm(data) {
  /* Destructuring
  let { ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } = data; (Mục đích là gọi lại data, lúc sau khỏi cần data.ten, chỉ cần innerHTML = ten)
  document.getElementById(`imgMonAn`).src = hinhMon;
  document.getElementById(`spMa`).innerHTML = ma;
  document.getElementById(`spTenMon`).innerHTML = ten;
  document.getElementById(`spLoaiMon`).innerHTML = loai;
  document.getElementById(`spGia`).innerHTML = giaMon;
  document.getElementById(`spKM`).innerHTML = khuyenMai;
  document.getElementById(`spTT`).innerHTML = tinhTrang;
  document.getElementById(`pMoTa`).innerHTML = moTa;
  document.getElementById(`spGiaKM`).innerText = data.tinhGiaKhuyenMai();
  */
  document.getElementById(`imgMonAn`).src = data.hinhMon;
  document.getElementById(`spMa`).innerHTML = data.ma;
  document.getElementById(`spTenMon`).innerHTML = data.ten;
  document.getElementById(`spLoaiMon`).innerHTML = data.loai;
  document.getElementById(`spGia`).innerHTML = data.giaMon;
  document.getElementById(`spKM`).innerHTML = data.khuyenMai;
  document.getElementById(`spTT`).innerHTML = data.tinhTrang;
  document.getElementById(`pMoTa`).innerHTML = data.moTa;
  document.getElementById(`spGiaKM`).innerText = data.tinhGiaKhuyenMai();
}
// 1 file chỉ được export default 1 lần duy nhất

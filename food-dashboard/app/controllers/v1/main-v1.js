import { layThongTinTuForm } from "./controller-v1.js";
import showThongTinLenForm from "./controller-v1.js";
import { MonAn } from "../../models/v1/monAnModel-v1.js";
// Thêm món ăn
document.getElementById(`btnThemMon`).addEventListener(`click`, function () {
  let data = layThongTinTuForm();
  let monAn = new MonAn(data);
  showThongTinLenForm(monAn);
});

import { renderFoodList } from "../../models/v2/controller-v2.js";
import { deleteFoodByID, getAllFood } from "../../service/service.js";

let fetchAllFoodService = () => {
  getAllFood()
    .then((res) => {
      console.log(res);
      renderFoodList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchAllFoodService();

function xoaMonAn(id) {
  deleteFoodByID(id)
    .then((res) => {
      Swal.fire(`Xoá món ăn thành công`);
      fetchAllFoodService();
    })
    .catch((err) => {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Xoá thất bại nha cđ",
        footer: '<a href="">Why do I have this issue?</a>',
      });
    });
}
window.xoaMonAn = xoaMonAn;

export const renderFoodList = (list) => {
  let contentHTML = ``;
  list.forEach((item) => {
    contentHTML += `
    <tr>
        <td>${item.ma}</td>
        <td>${item.ten}</td>
        <td>${item.loai ? `Mặn` : `Chay`}</td>
        <td>${item.gia}</td>
        <td>${item.phanTramKM}</td>
        <td>0</td>
        <td>${item.tinhTrang ? `Còn` : `Hết`}</td>
        <td>
            <button class="btn btn-danger" onclick="xoaMonAn(${
              item.ma
            })">Xoá</button>
            <button class="btn btn-warning" onclick="suaMonAn(${
              item.ma
            })">Sửa</button>
        </td>
    </tr>`;
  });
  document.getElementById(`tbodyFood`).innerHTML = contentHTML;
};

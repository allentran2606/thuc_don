// OOP ES6
export class MonAn {
  constructor(data) {
    this.ma = data.ma;
    this.ten = data.ten;
    this.loai = data.loai;
    this.giaMon = data.gia;
    this.khuyenMai = data.khuyenMai;
    this.tinhTrang = data.tinhTrang;
    this.hinhMon = data.hinhMon;
    this.moTa = data.moTa;
  }
  tinhGiaKhuyenMai = function () {
    return this.giaMon * (1 - this.khuyenMai / 100);
  };
}

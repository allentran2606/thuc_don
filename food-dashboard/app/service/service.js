let BASE_URL = `https://635f4b20ca0fe3c21a991e72.mockapi.io`;

export let getAllFood = () => {
  return axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  });
};
export let deleteFoodByID = (id) => {
  return axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  });
};
